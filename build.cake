#addin nuget:?package=Cake.Npm&version=0.16.0
#addin nuget:?package=Cake.Powershell&version=0.4.7

//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////
#region PREPARATION
var ComputerName = Environment.MachineName.ToLower();
var user = Environment.UserName;
var installerConfig = File(@"..\Setup\installer.config");
var dbPath = string.Empty;
var appName = XmlPeek(installerConfig, "/Configuration/Settings/appName/text()");
var db = XmlPeek(installerConfig, "/Configuration/Settings/databaseServer/text()");
var dbType = XmlPeek(installerConfig, "/Configuration/Settings/dbType/text()");
var adminDataBaseUserName = "sa";
var databaseUserName = XmlPeek(installerConfig, "/Configuration/Settings/databaseUserName/text()");
var databasePassword = XmlPeek(installerConfig, "/Configuration/Settings/databasePassword/text()");
/* Define computed properties on localproperties basics */
var workflowInstanceStoreDBName = $"SqlWorkflowInstanceStore_{appName}";

/* Define directories */
var frontEndDir = Directory("../Web/");
var SqlDirectory = Directory("./SQL");
var dbDeployScriptDir = Directory("../Database/Product/_Deployment");
var MIETToolDir = Directory("../../Tools/MetadataImportExportTool/bin");

/* Define files */
var coreSolutionFile = File("../ClientLifecycleManagement.sln");
var coreDBSolutionFile = File("../ClientLifecycleManagementDB.sln");

/* Define executables files */
var sqlCmd = File("C:/Program Files/Microsoft SQL Server/Client SDK/ODBC/130/Tools/Binn/SQLCMD.exe");
var configureElasticSearchIndices = File("../Elasticsearch/UpdateIndicesSQL.bat");
var fenergoInstaller = File("../Setup/FenergoInstaller/FenergoInstaller.exe");
var MIETTool = File("./../../Tools/MetadataImportExportTool/bin/MetadataImportExportTool.exe");
var RunScriptSQL = File("../Database/Product/_Deployment/RunScript.bat");
#endregion
//////////////////////////////////////////////////////////////////////
// Actions
//////////////////////////////////////////////////////////////////////
#region Actions
Action CoreSolutionBuildAction = () => {
    MSBuild(coreSolutionFile, new MSBuildSettings {
        Verbosity = Verbosity.Minimal,
        ToolVersion = MSBuildToolVersion.VS2017,
        Configuration = "Debug",
        PlatformTarget = PlatformTarget.MSIL});
};
Action DBSolutionBuildAction = () => {
     MSBuild(coreDBSolutionFile, settings => 
        settings.SetConfiguration("Release")
        .UseToolVersion(MSBuildToolVersion.Default)
        .SetVerbosity(Verbosity.Minimal)
        .WithTarget("Rebuild"));
};
Action NPMInstallAction = () => {
        var settings = new NpmInstallSettings
        {
            LogLevel = NpmLogLevel.Silent,
            WorkingDirectory = frontEndDir,
            Production = false
        };
        NpmInstall(settings);
};
Action NPMBuildFront = () => {   
    var settings = new  NpmRunScriptSettings
    {
        LogLevel = NpmLogLevel.Silent,
        WorkingDirectory = frontEndDir,
        ScriptName = "install-latest-ui"   
    };
    NpmRunScript(settings);
};
Action MetaDataAction = () => {
    StartProcess(MIETTool, new ProcessSettings { Arguments = "DevImport", WorkingDirectory = MIETToolDir });
};
Action SetUserAction = () => {
   StartProcess(
     sqlCmd, 
     new ProcessSettings { 
       Arguments = $"-S {db} -i Fenergo_UPDATE_USER.sql -U {adminDataBaseUserName} -P {databasePassword} -b -v DatabaseName=\"{appName}\" ComputerName=\"{ComputerName}\" DomainUser=\"{user}\"", 
       WorkingDirectory = SqlDirectory 
    }
   );
};
Action FenergoInstallerAction = () => {
    Information("Fenergo Installer...");
        StartProcess(
     fenergoInstaller, 
     new ProcessSettings { 
       Arguments = "-path=\"../\" -config=\"..\\setup\\installer.config\" --show-logs" 
    }
   );
};
Action SetElasticSearchAction = () => {
    Information("ElasticSearch updating indices...");
        StartProcess(
     configureElasticSearchIndices, 
     new ProcessSettings { 
       Arguments = $"{appName} true" 
    }
   );
};
Action SqlWorkflowInstanceStoreLogicAction = () => {
    Information("Deploying SqlWorkflowInstanceStoreLogic.sql...");
    StartProcess(
     sqlCmd, 
     new ProcessSettings { 
       Arguments = $"-S {db} -i SqlWorkflowInstanceStoreLogic.sql -U {adminDataBaseUserName} -P {databasePassword} -b -v DatabaseName=\"{workflowInstanceStoreDBName}\" DefaultPath=\"{dbPath}\"", 
       WorkingDirectory = dbDeployScriptDir 
    }
   );
};
Action SqlWorkflowInstanceStoreSchemaAction = () => {
    Information("Deploying SqlWorkflowInstanceStoreSchema.sql...");
    StartProcess(
     sqlCmd, 
     new ProcessSettings { 
       Arguments = $"-S {db} -i SqlWorkflowInstanceStoreSchema.sql -U {adminDataBaseUserName} -P {databasePassword} -b -v DatabaseName=\"{workflowInstanceStoreDBName}\" DefaultPath=\"{dbPath}\"",
       WorkingDirectory = dbDeployScriptDir 
    }
   );
};
Action SqlProductCreateAction = () => {
    Information("Deploying Product_Create.sql...");
    StartProcess(
     sqlCmd, 
     new ProcessSettings { 
       Arguments = $"-S {db} -i Product_Create.sql -U {adminDataBaseUserName} -P {databasePassword} -b -v DatabaseName=\"{appName}\" DefaultPath=\"{dbPath}\" IsJsonAuditFormat=false",
       WorkingDirectory = dbDeployScriptDir 
    }
   );
};

Action SqlCreateFenergoUser = () => {
    Information("Deploying CreateFenergoUser.sql...");
    StartProcess(
     sqlCmd, 
     new ProcessSettings { 
       Arguments = $"-S {db} -i CreateFenergoUser.sql -U {adminDataBaseUserName} -P {databasePassword} -b -v DatabaseName=\"{appName}\" WorkflowDatabaseName=\"{workflowInstanceStoreDBName}\" FenergoUserName={databaseUserName} FenergoUserPassword={databasePassword}",
       WorkingDirectory = dbDeployScriptDir 
    }
   );
};
Action GetSqlPathDBAction = () => {
    using(var process = StartAndReturnProcess(
        sqlCmd, 
     new ProcessSettings { 
       Arguments = $"-S {db} -h -1 -i Default_Data_Path.sql -U {adminDataBaseUserName} -P {databasePassword}",
       WorkingDirectory = SqlDirectory,
       RedirectStandardOutput = true 
    }))
    { 
        process.WaitForExit();
        var output = process.GetStandardOutput() ?? Enumerable.Empty<string>();
        var path = output.FirstOrDefault();
        if(string.IsNullOrEmpty(path)){
            throw new Exception("GetSqlPathDBAction Error");
        }
        dbPath = path.Trim();
    }
};

Action SetActiveMQ = () => {
     StartPowershellFile("../ActiveMq/SetupActiveMQQueue.ps1");
};
#endregion
//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////
#region Tasks
public void MakeParallel(IEnumerable<Action> actions,
    int maxDegreeOfParallelism = -1,
    System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken)) 
{                        

    var options = new ParallelOptions {
        MaxDegreeOfParallelism = maxDegreeOfParallelism,
        CancellationToken = cancellationToken
    };

    Parallel.Invoke(options, actions.ToArray());
}

Task("SetUser").Does(SetUserAction);

Task("Installer").Does(FenergoInstallerAction);

Task("SetDbUser").Does(SqlCreateFenergoUser);

Task("Build_Metadata").Does(MetaDataAction);

Task("NPMInstall").Does(NPMInstallAction);

Task("BuildFront").Does(NPMBuildFront);

Task("Build_CoreSolution").Does(CoreSolutionBuildAction);

Task("Build_DBSolution").Does(DBSolutionBuildAction);

Task("ElasticSearch").Does(SetElasticSearchAction);

Task("ActiveMQ")
    .Description("Setup ActiveMQ queues base on aplicaction name")
    .Does(SetActiveMQ);
Task("Deploy_DB")
    .Does(() => {
    switch (dbType)
    {
    case "Sql":
        GetSqlPathDBAction();
        SqlProductCreateAction();
        SqlWorkflowInstanceStoreSchemaAction();
        SqlWorkflowInstanceStoreLogicAction();
        SqlCreateFenergoUser();

        SetElasticSearchAction();
        break;
    case "Oracle":
        Information("Set Oracle DB");
        break;

    default:
        Warning(@"error  Database Settings option Sql / Oracle ");
        break;
    }
});
Task("BuildProjectsInParallel")
    .Does(() => 
    {
        var actionList = new List<Action>
        {
            CoreSolutionBuildAction,
            DBSolutionBuildAction,
            NPMBuildFront
        };
        MakeParallel(actionList);
    });

Task("SetProjectsInParallel")
    .Does(() => 
    {
        var actionList = new List<Action>
        {
            MetaDataAction,
            //SetActiveMQ,
            SetUserAction,
            FenergoInstallerAction 
        };
        MakeParallel(actionList);
    });
#endregion
//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
    .IsDependentOn("BuildProjectsInParallel")
    .IsDependentOn("Deploy_DB")
    .IsDependentOn("SetProjectsInParallel");
//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);