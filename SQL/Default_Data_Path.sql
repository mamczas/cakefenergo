DECLARE @default_log_path nvarchar(1000)
DECLARE @print_message   nvarchar (200)
DECLARE @version   nvarchar (50)
DECLARE @error int
DECLARE @on_debug_mode int
DECLARE @detection_mode nvarchar (50)

SET @default_log_path=NULL
SET @error=0
SET @on_debug_mode=0

SELECT @version=
CASE (select @@MICROSOFTVERSION / POWER(2,24))
WHEN 8 THEN N'SQL Server 2000'
WHEN 9 THEN N'SQL Server 2005'
WHEN 10 THEN N'SQL Server 2008'
WHEN 11 THEN N'SQL Server 2012'
WHEN 12 THEN N'SQL Server 2014'
ELSE N'OTHER'
END

BEGIN
	IF (@on_debug_mode=1) PRINT ('DEBUG: Default Log Path - Search with SERVERPROPERTY')
	SET @detection_mode=N'SERVERPROPERTY'
	SELECT @default_log_path = CONVERT(sysname,SERVERPROPERTY('InstanceDefaultLogPath'))
	if (@default_log_path IS NULL)
	BEGIN
		IF (@on_debug_mode=1) PRINT ('DEBUG: Default Log Path - Search with HKEY_LOCAL_MACHINE')
		SET @detection_mode=N'REGISTRY KEY'
		CREATE TABLE #instance_registry_path
		(
			instance_name sysname,
			registry_path sysname
		)

		INSERT INTO #instance_registry_path		
			EXEC master.sys.xp_instance_regenumvalues N'HKEY_LOCAL_MACHINE',N'SOFTWARE\\Microsoft\\Microsoft SQL Server\\Instance Names\\SQL'
		
		DECLARE @registry_path sysname
		DECLARE @instance_name sysname
		DECLARE @backslash_position int
		SET @instance_name=NULL
		SET @registry_path=NULL
		SET @backslash_position=0

		Select @backslash_position= CHARINDEX('\',@@SERVERNAME)
		IF (@backslash_position=0) -- Default instance
			SET @instance_name=N'MSSQLSERVER'
		ELSE
			SET @instance_name=SUBSTRING(@@SERVERNAME,@backslash_position+1,LEN(@@SERVERNAME))

		IF (@on_debug_mode=1)
		BEGIN
			SET @print_message= N'DEBUG: instance name: ' + @instance_name  
			PRINT(@print_message)
		END
		SELECT @registry_path=registry_path FROM #instance_registry_path WHERE @instance_name = instance_name
		IF (@on_debug_mode=1)
		BEGIN
			SET @print_message= N'DEBUG: registry path: ' + @registry_path  
			PRINT(@print_message)
		END
		DROP TABLE #instance_registry_path

		DECLARE @registry_key NVARCHAR(MAX)
		SET @registry_key= N'SOFTWARE\Microsoft\Microsoft SQL Server\'+@registry_path+N'\MSSQLServer'
		IF (@on_debug_mode=1)
		BEGIN
			SET @print_message= N'DEBUG: registry key: ' + @registry_key  
			PRINT(@print_message)
		END
	
		exec master.sys.xp_regread N'HKEY_LOCAL_MACHINE',@registry_key, N'DefaultLog',@default_log_path output

		IF(@default_log_path IS NULL)
		BEGIN
			IF (@on_debug_mode=1) PRINT ('DEBUG: Default Log Path - Search with USER-DATABASE')
			DECLARE @database_name sysname;
			SET @database_name=NULL
			SET @detection_mode=N'USER-DATABASE'
			SELECT TOP 1 @database_name=name from sys.databases where database_id>5;
			IF (@database_name IS NULL)
			BEGIN
				PRINT('WARNING: No user-database in this instance');
			END
			ELSE
			BEGIN
				IF (@on_debug_mode=1)
				BEGIN
					SET @print_message= N'DEBUG: user-database name: ' + @database_name
					PRINT(@print_message);
				END
				ELSE
				BEGIN
					DECLARE @sqlexec nvarchar (200);
					--use sys.database_files to find the path
					SET @sqlexec = N'use ['+@database_name+'];select TOP 1 @log_path=physical_name from sys.database_files where type=1;';
					EXEC sp_executesql @sqlexec, N'@log_path nvarchar(max) OUTPUT',@log_path=@default_log_path OUTPUT;
					IF (@on_debug_mode=1)
					BEGIN
						SET @print_message= N'DEBUG: physical name: ' + @default_log_path
						PRINT(@print_message);
					END
					-- Find the path from first character to the last '\'
					SET @default_log_path=REVERSE(STUFF(REVERSE(@default_log_path),1,CHARINDEX('\',REVERSE(@default_log_path)),''))
					IF (@on_debug_mode=1)
					BEGIN
						SET @print_message= N'DEBUG: user database path: ' + @default_log_path
						PRINT(@print_message);
					END
				END
			END
			IF(@default_log_path IS NULL)
			BEGIN
				IF (@on_debug_mode=1) PRINT ('DEBUG: Default Log Path - Search with MASTER DATABASE')
				SET @detection_mode = N'MASTER DATABASE'
				--use master.sys.database_files to find the path
				select TOP 1 @default_log_path = physical_name from master.sys.database_files df where df.type=1;
				IF (@on_debug_mode=1)
					BEGIN
						SET @print_message= N'DEBUG: physical name: ' + @default_log_path
						PRINT(@print_message);
					END
				SET @default_log_path=REVERSE(STUFF(REVERSE(@default_log_path),1,CHARINDEX('\',REVERSE(@default_log_path)),''))
				IF (@on_debug_mode=1)
				BEGIN
					SET @print_message= N'DEBUG: default master log path name: ' + @default_log_path
					PRINT(@print_message);
				END
			END
		END
	END
	IF (@default_log_path IS NULL)
	BEGIN
		SET @error=@@ERROR
		IF (@error<>0)
		BEGIN
			SET @print_message=N'Error N� ' + RTRIM (CAST (@error AS NVARCHAR (10))) + N' - Message: '+ERROR_MESSAGE();
			PRINT(@print_message)
		END
		ELSE
		BEGIN
			PRINT(N'No error number is generated')
		END
	END
	ELSE 
	BEGIN
		SET @print_message=@version+N' - Detection mode: '+ @detection_mode + N' - Path: ' + @default_log_path
		IF (@on_debug_mode=1) PRINT(@print_message)
	END
END
SELECT @default_log_path as [Default Log Path]